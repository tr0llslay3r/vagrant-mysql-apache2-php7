#!/usr/bin/env bash

sudo apt-get update
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get install -y vim curl python-software-properties software-properties-common
sudo LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
sudo LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/apache2
sudo apt-get update
echo --- INSTALLING MYSQL-SERVER
sudo apt-get -y install mysql-server
echo --- CONFIGURING MYSQL-SERVER
sed -i "s/^bind-address/#bind-address/" /etc/mysql/my.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES; SET GLOBAL max_connect_errors=10000;"
mysql -u root -proot < /vagrant/dump.sql
sudo /etc/init.d/mysql restart
echo --- INSTALLING APACHE
sudo apt-get -y install apache2
echo --- INSTALLING PHP 7
sudo apt-get install -y libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-json php7.0-xml php-mbstring php7.0-zip
if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant /var/www/html
fi
sudo echo extension=mysqli.so >> /etc/php/7.0/apache2/php.ini
sudo service apache2 restart
