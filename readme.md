# A vagrant machine with apache2, mysql and php7 installed

### Requirements:
- Virtualbox (Hyper-V and VMware should work too, but not tested yet)
- Vagrant

### Usage:
```sh
"vagrant up"
```

### Result:
- /dump.sql will be imported into the mysql database
- mysql is ready on localhost:3306 with root:root (example db included)
- http://localhost:4567 will host your root working dorectory
